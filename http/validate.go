package http

import (
	"fmt"
	"io"
	"net/http"
)

// Validation function when we want to surface the errors up to the caller
func (c *Client) Validate(response *http.Response, responseErr error) error {
	// Parsing our register/deregister response and confirming no errors
	if responseErr != nil {
		return fmt.Errorf("error connecting to remote: %s", responseErr.Error())
	}
	if response.StatusCode >= 200 && response.StatusCode <= 299 {
		return nil
	} else {
		body, err := io.ReadAll(response.Body)
		if err != nil {
			return fmt.Errorf("%d/HTTP - failure while reading response body from remote: %s", response.StatusCode, err.Error())
		}
		return fmt.Errorf("%d/HTTP - %s", response.StatusCode, string(body))
	}
}
