package http

import (
	"fmt"
	"io"
	"net/http"
)

func (c *Client) Call(method string, body io.Reader) (*http.Response, error) {
	if len(c.Url) == 0 {
		return &http.Response{}, fmt.Errorf("HTTP request URL must be set before calling a method function")
	}

	// Generating the HTTP request
	request, err := http.NewRequest(method, c.Url, body)
	if err != nil {
		return &http.Response{}, err
	}

	// Setting our headers
	for _, header := range c.Headers {
		request.Header.Add(header.Key, header.Value)
	}

	// Making the call returning
	return c.HttpClient.Do(request)
}

// Wrapper function for GET method Call()
func (c *Client) Get() ([]byte, error) {
	response, err := c.Call("GET", nil)

	err = c.Validate(response, err)
	if err != nil {
		return nil, err
	}

	results, err := c.ReadResponse(response)
	if err != nil {
		return nil, err
	}

	return results, nil
}

// Wrapper function for POST method Call()
func (c *Client) Post(body io.Reader) ([]byte, error) {
	response, err := c.Call("POST", body)

	err = c.Validate(response, err)
	if err != nil {
		return nil, err
	}

	results, err := c.ReadResponse(response)
	if err != nil {
		return nil, err
	}

	return results, nil
}

// Wrapper function for DELETE method Call()
func (c *Client) Delete() ([]byte, error) {
	response, err := c.Call("DELETE", nil)

	err = c.Validate(response, err)
	if err != nil {
		return nil, err
	}

	results, err := c.ReadResponse(response)
	if err != nil {
		return nil, err
	}

	return results, nil
}
