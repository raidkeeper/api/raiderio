package http

import (
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
)

type Header struct {
	Key   string
	Value string
}

type Client struct {
	Headers    []Header
	HttpClient *http.Client
	Method     string
	Url        string
}

func New(config *Config) *Client {
	c := &Client{}

	config.Defaults()

	transport := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: config.Insecure,
		},
	}

	c.HttpClient = &http.Client{
		Timeout:   config.Timeout,
		Transport: transport,
	}
	return c
}

func (c *Client) AddHeader(key string, value string) {
	c.Headers = append(c.Headers, Header{
		Key:   key,
		Value: value,
	})
}

// Read body
func (c *Client) ReadResponse(response *http.Response) ([]byte, error) {
	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response: %s", err)
	}
	return bodyBytes, nil
}
