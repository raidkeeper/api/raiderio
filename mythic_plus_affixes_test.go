package raiderio

import (
	"testing"
)

func TestMythicPlusAffixes(t *testing.T) {
	r, err := CreateRaiderioClient()
	if err != nil {
		t.Error(err)
	}

	affixes, err := r.GetMythicPlusAffixes()
	if err != nil {
		t.Error(err)
	}

	if len(affixes.AffixDetails) != 4 {
		t.Errorf("expected 4 affixes, got %d", len(affixes.AffixDetails))
	}
}
