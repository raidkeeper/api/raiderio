package raiderio

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/raidkeeper/api/raiderio/schemas"
)

var characterProfileScopes []string = []string{
	"gear",
	"talents",
	"guild",
	"raid_progression",
	"mythic_plus_weekly_highest_level_runs",
	"mythic_plus_previous_weekly_highest_level_runs",
	"mythic_plus_recent_runs",
	"mythic_plus_ranks",
}

func (r *Raiderio) CharacterProfile(realm string, name string) (*schemas.CharacterProfileResponse, error) {
	scopes := strings.Join(characterProfileScopes, "%2C")
	args := fmt.Sprintf("fields=%s&region=%s&realm=%s&name=%s", scopes, r.Config.Region, realm, name)
	r.BuildRequest(endpoints["character-profile"], args)

	resp, err := r.Client.Get()
	if err != nil {
		return &schemas.CharacterProfileResponse{}, err
	}

	o := schemas.CharacterProfileResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}
