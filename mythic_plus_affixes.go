package raiderio

import (
	"encoding/json"
	"fmt"

	"gitlab.com/raidkeeper/api/raiderio/schemas"
)

func (r *Raiderio) GetMythicPlusAffixes() (*schemas.MythicPlusAffixesResponse, error) {
	args := fmt.Sprintf("region=%s&locale=%s", r.Config.Region, r.Config.Locale)
	r.BuildRequest(endpoints["mplus-affixes"], args)

	resp, err := r.Client.Get()
	if err != nil {
		return &schemas.MythicPlusAffixesResponse{}, err
	}

	o := schemas.MythicPlusAffixesResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}
