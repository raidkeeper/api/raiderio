package raiderio

import (
	"encoding/json"
	"fmt"

	"gitlab.com/raidkeeper/api/raiderio/schemas"
)

func (r *Raiderio) Periods() (*schemas.PeriodsResponse, error) {
	args := ""
	r.BuildRequest(endpoints["periods"], args)

	resp, err := r.Client.Get()
	if err != nil {
		return &schemas.PeriodsResponse{}, err
	}

	o := schemas.PeriodsResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}

func (r *Raiderio) GetRegionalPeriods(region string) (*schemas.RegionPeriodSchema, error) {
	s, err := r.Periods()
	if err != nil {
		return &schemas.RegionPeriodSchema{}, err
	}
	for _, r := range s.Periods {
		if r.Region == region {
			return &r, nil
		}
	}
	return &schemas.RegionPeriodSchema{}, fmt.Errorf("period not found for given region %s", region)
}

func (r *Raiderio) GetCurrentPeriod() (*schemas.Period, error) {
	p, err := r.GetRegionalPeriods(r.Config.Region)
	if err != nil {
		return &schemas.Period{}, err
	}
	return &p.Current, nil
}

func (r *Raiderio) GetPreviousPeriod() (*schemas.Period, error) {
	p, err := r.GetRegionalPeriods(r.Config.Region)
	if err != nil {
		return &schemas.Period{}, err
	}
	return &p.Previous, nil
}

func (r *Raiderio) GetNextPeriod() (*schemas.Period, error) {
	p, err := r.GetRegionalPeriods(r.Config.Region)
	if err != nil {
		return &schemas.Period{}, err
	}
	return &p.Previous, nil
}
