package raiderio

import (
	"encoding/json"
	"fmt"

	"gitlab.com/raidkeeper/api/raiderio/schemas"
)

func (r *Raiderio) GetMythicPlusRunDetails(season string, id string) (*schemas.MythicPlusRunDetailsResponse, error) {
	args := fmt.Sprintf("season=%s&id=%s", season, id)
	r.BuildRequest(endpoints["mplus-run-details"], args)

	resp, err := r.Client.Get()
	if err != nil {
		return &schemas.MythicPlusRunDetailsResponse{}, err
	}

	o := schemas.MythicPlusRunDetailsResponse{}
	err = json.Unmarshal(resp, &o)
	return &o, err
}
