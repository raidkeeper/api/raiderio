package raiderio

import (
	"testing"
	"time"
)

func TestCurrentPeriod(t *testing.T) {
	r, err := CreateRaiderioClient()
	if err != nil {
		t.Error(err)
	}

	period, err := r.GetCurrentPeriod()
	if err != nil {
		t.Error(err)
	}

	if period.ID == 0 {
		t.Errorf("No period fetched")
	}

	if period.Start.Unix() >= time.Now().Unix() {
		t.Errorf(
			"The current period was not fetched properly. Start time %d exceeds current time %d",
			period.Start.Unix(),
			time.Now().Unix(),
		)
	}
}
