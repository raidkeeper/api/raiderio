package raiderio

func CreateRaiderioClient() (*Raiderio, error) {
	return New(&Config{
		Locale: "en",
		Region: "us",
	})
}
