WORKDIR=$$(pwd)
.PHONY: test

default: test

test: ## Run go tests and create coverage report 
	@rm -f coverage.txt profile.out
	@rm -f gosec-report.json
	go test ./... -race -coverprofile=coverage.txt -covermode=atomic