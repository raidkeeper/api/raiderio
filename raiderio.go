package raiderio

import (
	"fmt"
	"strings"

	"gitlab.com/raidkeeper/api/raiderio/http"
)

var (
	apiVersion string = "v1"
	baseUrl    string = "https://raider.io/api"

	endpoints map[string]string = map[string]string{
		"character-profile": "characters/profile",
		"periods":           "periods",
		"mplus-affixes":     "mythic-plus/affixes",
		"mplus-run-details": "mythic-plus/run-details",
	}
)

type Raiderio struct {
	Config *Config
	Client *http.Client
}

func New(config *Config) (*Raiderio, error) {
	r := &Raiderio{
		Config: config,
	}

	r.Client = http.New(&http.Config{})
	return r, nil
}

func (r *Raiderio) Slugify(text string) string {
	text = strings.ReplaceAll(text, " ", "-")
	text = strings.ReplaceAll(text, "'", "")
	return strings.ToLower(text)
}

func (r *Raiderio) BuildRequest(endpoint string, args string) {
	r.Client.Url = fmt.Sprintf(
		"%s/%s/%s?%s",
		baseUrl,
		apiVersion,
		endpoint,
		args,
	)
}
