package raiderio

import (
	"testing"
)

func TestCharacterProfile(t *testing.T) {
	r, err := CreateRaiderioClient()
	if err != nil {
		t.Error(err)
	}

	profile, err := r.CharacterProfile("sargeras", "ashkrall")
	if err != nil {
		t.Error(err)
	}

	if profile.Name != "Ashkrall" {
		t.Errorf("Expected profile.Name equal to Ashkrall, found %s", profile.Name)
	}
}
