package raiderio

import (
	"testing"
)

func TestMythicPlusRunDetails(t *testing.T) {
	r, err := CreateRaiderioClient()
	if err != nil {
		t.Error(err)
	}

	run, err := r.GetMythicPlusRunDetails("season-df-1", "10438815")
	if err != nil {
		t.Error(err)
	}

	if run.TimeRemainingMs == 0 {
		t.Error("expected actual run completion timer, got 0")
	}

	if len(run.Roster) != 5 {
		t.Errorf("expected 5 keystone roster members, got %d", len(run.Roster))
	}
}
