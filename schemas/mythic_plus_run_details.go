package schemas

import "time"

type MythicPlusRunDetailsResponse struct {
	Season              string            `json:"season"`
	Status              string            `json:"status"`
	Dungeon             Dungeon           `json:"dungeon"`
	KeystoneRunID       int               `json:"keystone_run_id"`
	MythicLevel         int               `json:"mythic_level"`
	ClearTimeMs         int               `json:"clear_time_ms"`
	KeystoneTimeMs      int               `json:"keystone_time_ms"`
	CompletedAt         time.Time         `json:"completed_at"`
	NumChests           int               `json:"num_chests"`
	TimeRemainingMs     int               `json:"time_remaining_ms"`
	LoggedRunID         interface{}       `json:"logged_run_id"`
	WeeklyModifiers     []WeeklyModifiers `json:"weekly_modifiers"`
	NumModifiersActive  int               `json:"num_modifiers_active"`
	Faction             string            `json:"faction"`
	DeletedAt           interface{}       `json:"deleted_at"`
	Score               float64           `json:"score"`
	LoggedDetails       interface{}       `json:"logged_details"`
	KeystoneTeamID      int               `json:"keystone_team_id"`
	KeystonePlatoonID   interface{}       `json:"keystone_platoon_id"`
	IsTournamentProfile bool              `json:"isTournamentProfile"`
	Roster              []Roster          `json:"roster"`
	LoggedSources       []interface{}     `json:"loggedSources"`
}

type Dungeon struct {
	ID              int    `json:"id"`
	Name            string `json:"name"`
	ShortName       string `json:"short_name"`
	Slug            string `json:"slug"`
	ExpansionID     int    `json:"expansion_id"`
	Patch           string `json:"patch"`
	KeystoneTimerMs int    `json:"keystone_timer_ms"`
	NumBosses       int    `json:"num_bosses"`
}
type WeeklyModifiers struct {
	ID          int    `json:"id"`
	Icon        string `json:"icon"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
type Class struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}
type Race struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Slug    string `json:"slug"`
	Faction string `json:"faction"`
}
type Spec struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Slug    string `json:"slug"`
	ClassID int    `json:"class_id"`
	Role    string `json:"role"`
	IsMelee bool   `json:"is_melee"`
}
type Realm struct {
	ID               int         `json:"id"`
	ConnectedRealmID int         `json:"connectedRealmId"`
	Name             string      `json:"name"`
	AltName          interface{} `json:"altName"`
	Slug             string      `json:"slug"`
	AltSlug          string      `json:"altSlug"`
	Locale           string      `json:"locale"`
	IsConnected      bool        `json:"isConnected"`
}
type Region struct {
	Name      string `json:"name"`
	Slug      string `json:"slug"`
	ShortName string `json:"short_name"`
}
type Spell struct {
	ID     int         `json:"id"`
	Name   string      `json:"name"`
	Icon   string      `json:"icon"`
	School int         `json:"school"`
	Rank   interface{} `json:"rank"`
}
type Entries struct {
	ID       int   `json:"id"`
	Type     int   `json:"type"`
	MaxRanks int   `json:"maxRanks"`
	Spell    Spell `json:"spell"`
}
type Node struct {
	ID        int       `json:"id"`
	TreeID    int       `json:"treeId"`
	Type      int       `json:"type"`
	Entries   []Entries `json:"entries"`
	Important bool      `json:"important"`
	PosX      int       `json:"posX"`
	PosY      int       `json:"posY"`
	Row       int       `json:"row"`
	Col       int       `json:"col"`
}
type Loadout struct {
	Node       Node `json:"node"`
	EntryIndex int  `json:"entryIndex"`
	Rank       int  `json:"rank"`
}
type TalentLoadout struct {
	SpecID      int       `json:"specId"`
	Loadout     []Loadout `json:"loadout"`
	LoadoutText string    `json:"loadoutText"`
}
type Character struct {
	ID                  int           `json:"id"`
	PersonaID           int           `json:"persona_id"`
	Name                string        `json:"name"`
	Class               Class         `json:"class"`
	Race                Race          `json:"race"`
	Faction             string        `json:"faction"`
	Level               int           `json:"level"`
	Spec                Spec          `json:"spec"`
	Path                string        `json:"path"`
	Realm               Realm         `json:"realm"`
	Region              Region        `json:"region"`
	Stream              interface{}   `json:"stream"`
	RecruitmentProfiles []interface{} `json:"recruitmentProfiles"`
	TalentLoadout       TalentLoadout `json:"talentLoadout"`
}
type Guild struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Faction string `json:"faction"`
	Realm   Realm  `json:"realm"`
	Region  Region `json:"region"`
	Path    string `json:"path"`
}
type Corruption struct {
	Added     int           `json:"added"`
	Resisted  int           `json:"resisted"`
	Total     int           `json:"total"`
	CloakRank int           `json:"cloakRank"`
	Spells    []interface{} `json:"spells"`
}

type EquippedItem struct {
	ItemID           int           `json:"item_id"`
	ItemLevel        int           `json:"item_level"`
	Enchant          int           `json:"enchant"`
	Icon             string        `json:"icon"`
	Name             string        `json:"name"`
	ItemQuality      int           `json:"item_quality"`
	IsLegendary      bool          `json:"is_legendary"`
	IsAzeriteArmor   bool          `json:"is_azerite_armor"`
	AzeritePowers    []interface{} `json:"azerite_powers"`
	Corruption       Corruption    `json:"corruption"`
	DominationShards []interface{} `json:"domination_shards"`
	Tier             string        `json:"tier"`
	Gems             []int         `json:"gems"`
	Bonuses          []int         `json:"bonuses"`
}

type EquippedItems struct {
	Head     EquippedItem `json:"head"`
	Neck     EquippedItem `json:"neck"`
	Shoulder EquippedItem `json:"shoulder"`
	Back     EquippedItem `json:"back"`
	Chest    EquippedItem `json:"chest"`
	Waist    EquippedItem `json:"waist"`
	Shirt    EquippedItem `json:"shirt"`
	Wrist    EquippedItem `json:"wrist"`
	Hands    EquippedItem `json:"hands"`
	Legs     EquippedItem `json:"legs"`
	Feet     EquippedItem `json:"feet"`
	Finger1  EquippedItem `json:"finger1"`
	Finger2  EquippedItem `json:"finger2"`
	Trinket1 EquippedItem `json:"trinket1"`
	Trinket2 EquippedItem `json:"trinket2"`
	Mainhand EquippedItem `json:"mainhand"`
	Offhand  EquippedItem `json:"offhand"`
}
type Items struct {
	UpdatedAt         time.Time     `json:"updated_at"`
	ItemLevelEquipped int           `json:"item_level_equipped"`
	ItemLevelTotal    int           `json:"item_level_total"`
	ArtifactTraits    int           `json:"artifact_traits"`
	Corruption        Corruption    `json:"corruption"`
	Items             EquippedItems `json:"items"`
}
type Ranks struct {
	Score  float64 `json:"score"`
	World  int     `json:"world"`
	Region int     `json:"region"`
	Realm  int     `json:"realm"`
}
type Roster struct {
	Character    Character   `json:"character"`
	OldCharacter interface{} `json:"oldCharacter"`
	IsTransfer   bool        `json:"isTransfer"`
	Guild        Guild       `json:"guild"`
	Role         string      `json:"role"`
	Items        Items       `json:"items"`
	Ranks        Ranks       `json:"ranks"`
}
