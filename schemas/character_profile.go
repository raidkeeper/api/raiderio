package schemas

import "time"

type CharacterProfileResponse struct {
	Name            string              `json:"name"`
	Race            string              `json:"race"`
	Class           string              `json:"class"`
	ActiveSpecName  string              `json:"active_spec_name"`
	ActiveSpecRole  string              `json:"active_spec_role"`
	Gender          string              `json:"gender"`
	Faction         string              `json:"faction"`
	Region          string              `json:"region"`
	Realm           string              `json:"realm"`
	ProfileURL      string              `json:"profile_url"`
	Gear            CharacterGearSchema `json:"gear"`
	RaidProgression struct {
		VaultOfTheIncarnates         RaidProgressionSchema `json:"vault-of-the-incarnates"`
		FatedSepulcherOfTheFirstOnes RaidProgressionSchema `json:"fated-sepulcher-of-the-first-ones"`
		FatedSanctumOfDomination     RaidProgressionSchema `json:"fated-sanctum-of-domination"`
		FatedCastleNathria           RaidProgressionSchema `json:"fated-castle-nathria"`
		SepulcherOfTheFirstOnes      RaidProgressionSchema `json:"sepulcher-of-the-first-ones"`
		SanctumOfDomination          RaidProgressionSchema `json:"sanctum-of-domination"`
		CastleNathria                RaidProgressionSchema `json:"castle-nathria"`
		NyalothaTheWakingCity        RaidProgressionSchema `json:"nyalotha-the-waking-city"`
		TheEternalPalace             RaidProgressionSchema `json:"the-eternal-palace"`
		CrucibleOfStorms             RaidProgressionSchema `json:"crucible-of-storms"`
		BattleOfDazaralor            RaidProgressionSchema `json:"battle-of-dazaralor"`
		Uldir                        RaidProgressionSchema `json:"uldir"`
		AntorusTheBurningThrone      RaidProgressionSchema `json:"antorus-the-burning-throne"`
		TombOfSargeras               RaidProgressionSchema `json:"tomb-of-sargeras"`
		TheNighthold                 RaidProgressionSchema `json:"the-nighthold"`
		TrialOfValor                 RaidProgressionSchema `json:"trial-of-valor"`
		TheEmeraldNightmare          RaidProgressionSchema `json:"the-emerald-nightmare"`
	} `json:"raid_progression"`
	MythicPlusRanks                     MythicPlusRankGroupSchema `json:"mythic_plus_ranks"`
	MythicPlusScores                    MythicPlusScoresSchema    `json:"mythic_plus_scores"`
	PreviousMythicPlusRanks             MythicPlusRankGroupSchema `json:"previous_mythic_plus_ranks"`
	PreviousMythicPlusScores            MythicPlusScoresSchema    `json:"previous_mythic_plus_scores"`
	MythicPlusRecentRuns                []KeystoneRunSchema       `json:"mythic_plus_recent_runs"`
	MythicPlusBestRuns                  []KeystoneRunSchema       `json:"mythic_plus_best_runs"`
	MythicPlusAlternateRuns             []KeystoneRunSchema       `json:"mythic_plus_alternate_runs"`
	MythicPlusWeeklyHighestRuns         []KeystoneRunSchema       `json:"mythic_plus_weekly_highest_level_runs"`
	MythicPlusPreviousWeeklyHighestRuns []KeystoneRunSchema       `json:"mythic_plus_previous_weekly_highest_level_runs"`
}

type CharacterGearSchema struct {
	ItemLevelEquipped int `json:"item_level_equipped"`
	ItemLevelTotal    int `json:"item_level_total"`
	ArtifactTraits    int `json:"artifact_traits"`
}

type RaidProgressionSchema struct {
	Summary            string `json:"summary"`
	TotalBosses        int    `json:"total_bosses"`
	NormalBossesKilled int    `json:"normal_bosses_killed"`
	HeroicBossesKilled int    `json:"heroic_bosses_killed"`
	MythicBossesKilled int    `json:"mythic_bosses_killed"`
}

type MythicPlusRankGroupSchema struct {
	Overall     MythicPlusRankSchema `json:"overall"`
	Tank        MythicPlusRankSchema `json:"tank"`
	Healer      MythicPlusRankSchema `json:"healer"`
	Dps         MythicPlusRankSchema `json:"dps"`
	Class       MythicPlusRankSchema `json:"class"`
	ClassTank   MythicPlusRankSchema `json:"class_tank"`
	ClassHealer MythicPlusRankSchema `json:"class_healer"`
	ClassDps    MythicPlusRankSchema `json:"class_dps"`
}

type MythicPlusRankSchema struct {
	World  int `json:"world"`
	Region int `json:"region"`
	Realm  int `json:"realm"`
}

type MythicPlusScoresSchema struct {
	All    int `json:"all"`
	Dps    int `json:"dps"`
	Healer int `json:"healer"`
	Tank   int `json:"tank"`
}

type KeystoneRunSchema struct {
	Dungeon             string    `json:"dungeon"`
	ShortName           string    `json:"short_name"`
	MythicLevel         int       `json:"mythic_level"`
	CompletedAt         time.Time `json:"completed_at"`
	ClearTimeMs         int       `json:"clear_time_ms"`
	NumKeystoneUpgrades int       `json:"num_keystone_upgrades"`
	Score               float64   `json:"score"`
	URL                 string    `json:"url"`
}
