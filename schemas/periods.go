package schemas

import (
	"time"
)

type PeriodsResponse struct {
	Periods []RegionPeriodSchema `json:"periods"`
}

type RegionPeriodSchema struct {
	Region   string `json:"region"`
	Previous Period `json:"previous"`
	Current  Period `json:"current"`
	Next     Period `json:"next"`
}

type Period struct {
	ID    int       `json:"period"`
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
}
